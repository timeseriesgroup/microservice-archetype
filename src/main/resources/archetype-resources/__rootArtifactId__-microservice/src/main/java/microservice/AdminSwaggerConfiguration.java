package nl.timeseries.training.microservice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static java.util.Collections.singletonList;

@Configuration
@EnableSwagger2
public class AdminSwaggerConfiguration {

	@Bean
	public Docket adminDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(adminApiInfo())
				.groupName("admin")
				.securitySchemes(singletonList(new BasicAuth("admin")))
				.securityContexts(singletonList(SecurityContext.builder()
						.securityReferences(singletonList(SecurityReference.builder()
								.reference("admin")
								.scopes(new AuthorizationScope[] {
										new AuthorizationScope("admin", "Admin endpoints")
								})
								.build()))
						.forPaths(PathSelectors.any())
						.build()))
				.select()
				.paths(PathSelectors.regex("/admin/[^.]*"))
				.build();
	}

	private ApiInfo adminApiInfo() {
		return new ApiInfoBuilder()
				.title("Admin Endpoints")
				.description("Use to explore the server state.")
				.contact(new Contact("TimeSeries", "http://www.timeseries.nl", "info@timeseries.nl"))
				.build();
	}
}
